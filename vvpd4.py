import sys


def proverka(cipher):
    count = 0
    for i in range(len(cipher)):
        if cipher[i] == "." or cipher[i] == "-":
            continue
        else:
            count += 1
    if count != 0:
        print("Impossible to decode")
        sys.exit()

def main():    
    cipher = str(input("Введите шифр: "))
    proverka(cipher)
    meowpher = []
    if len(cipher) == 0:
        meowpher = ''
    else:
        if len(cipher) % 3 != 0:
            meowpher = 'Impossible to decode'
            print(meowpher)
        else:
            for i in range(3, len(cipher)+3, 3):
                if cipher[i-3:i] == "---":
                    meowpher.append("O")
                elif cipher[i-3:i] == "--.":
                    meowpher.append("E")
                elif cipher[i-3:i] == "-.-":
                    meowpher.append("W")
                elif cipher[i-3:i] == "-..":
                    meowpher.append("M")
                elif cipher[i-3:i] == ".--":
                    meowpher.append('C')
                elif cipher[i-3:i] == ".-.":
                    meowpher.append('A')
                elif cipher[i-3:i] == "..-":
                    meowpher.append('U')
                elif cipher[i-3:i] == "...":
                    meowpher.append('Q')
            meowpher = "".join(meowpher)
            print(meowpher)

if __name__ == "__main__":
    main()
